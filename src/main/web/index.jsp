<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
  <c:set var="title" value="Cash Register" scope="session"/>
  <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>
<body>
<c:if test="${not empty user}">
  <c:redirect url="WEB-INF/jsp/main.jsp"/>
</c:if>

<c:if test="${empty user and title ne 'Login'}">
  <c:redirect url="login.jsp"/>
</c:if>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
