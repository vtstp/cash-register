<html>
<%@ include file="WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="WEB-INF/jspf/directive/taglib.jspf" %>
<c:set var="title" value="Login" scope="page"/>
<%@ include file="WEB-INF/jspf/head.jspf" %>
<body>
<%@include file="WEB-INF/jspf/header.jspf"%>
<div id="login">
    <h2 class="block-title">Welcome!</h2>
    <br/>
    <form id="login-form" action="controller" method="post">
        <input type="hidden" name="command" value="login"/>
        <label for="login">Login</label>
        <input type="text" name="login" placeholder="Your login..."/>
        <label for="password">Password</label>
        <input type="password" name="password" placeholder="Your password..."/>
        <br/>
        <input type="submit" value="Log In">
    </form>
</div>
<%@ include file="WEB-INF/jspf/footer.jspf" %>
</body>
</html>