<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Receipt List" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>
<c:choose>
    <c:when test="${fn:length(receiptList) == 0}">No receipts yet</c:when>

    <c:otherwise>
        <table>
            <thead>
            <tr>
                <th>id</th>
                <th>Date</th>
                <th>Time</th>
                <th>User</th>
                <th>Total</th>
                <th>Status</th>
                <th>Select</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="receipt" items="${receiptList}">
                <tr class="receipt-row">
                    <td>${receipt.id}</td>
                    <td>${receipt.date}</td>
                    <td>${receipt.time}</td>
                    <td>${receipt.userLogin}</td>
                    <td>${receipt.totalPrice}</td>
                    <td>${receipt.status}</td>
                    <td>
                        <c:url value="controller" var="showOrder">
                            <c:param name="command" value="showOrder"/>
                            <c:param name="receiptId" value="${receipt.id}"/>
                            <c:param name="totalPrice" value="${receipt.totalPrice}"/>
                            <c:param name="status" value="${receipt.status}"/>
                        </c:url>
                        <form class="show-order-form" method="post" action="${showOrder}">
                            <input type="submit" value="Select" <c:if test="${receipt.status == 'canceled'}">disabled</c:if>/>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </c:otherwise>
</c:choose>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>