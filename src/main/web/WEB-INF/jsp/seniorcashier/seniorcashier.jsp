<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Senior Cashier" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>

<body>

<div>
    <c:url value="controller" var="showAllReceipts">
        <c:param name="command" value="showAllReceipts"/>
    </c:url>
    <form id="show-receipts-form" method="post" action="${showAllReceipts}">
        <button type="submit" id="showReceiptsButton" form="show-receipts-form">Show all receipts</button>
    </form>
</div>

<jsp:include page="/WEB-INF/jspf/footer.jspf"/>


</body>
</html>