<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ page session="false" %>

<html>
<c:set var="title" value="Product List" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>

<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Select</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="product" items="${productList}">
        <tr>
            <td>${product.id}</td>
            <td>${product.name}</td>
            <td>${product.quantity}</td>
            <td>${product.price}</td>
            <td>
                <c:url value="controller" var="showProduct">
                    <c:param name="command" value="showProduct"/>
                    <c:param name="productId" value="${product.id}"/>
                    <c:param name="source" value="productList"/>
                </c:url>
                <form class="select-product-form" method="post" action="${showProduct}">
                    <input type="submit" value="View"/>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>