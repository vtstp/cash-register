<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Cashier" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>

<body>
<ul>
    <li>
        <div>
            <c:url value="controller" var="createOrder">
                <c:param name="command" value="createOrder"/>
            </c:url>
            <form id="create-order-form" method="post" action="${createOrder}">
                <button type="submit" form="create-order-form"
                        <c:if test="${not empty openedOrder}">disabled</c:if>>Create New Order
                </button>
            </form>
        </div>
    </li>
    <li>
        <div>
            <c:url value="controller" var="showOrder">
                <c:param name="command" value="showOrder"/>
            </c:url>
            <form id="show-order-form" method="post" action="${showOrder}">
                <button type="submit" id="showOrderButton" form="show-order-form"
                        <c:if test="${empty openedOrder}">disabled</c:if>>Show Current Order
                </button>
            </form>
        </div>
    </li>
</ul>

<jsp:include page="/WEB-INF/jspf/footer.jspf"/>

</body>
</html>