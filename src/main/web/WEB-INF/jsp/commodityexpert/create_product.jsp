<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>
<head>
    <c:set var="title" value="New Product" scope="page"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>

<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>
<h3>New Product: </h3>
<div>
    <c:url value="controller" var="createProduct">
        <c:param name="command" value="createProduct"/>
    </c:url>
    <form id="new-product-form" method="post" action="${createProduct}">
        <label for="new-product-name">Product name:</label><br>
        <input type="text" id="new-product-name" name="newProductName" placeholder="ProductName"><br>
        <label for="new-product-price">Price:</label><br>
        <input type="number" id="new-product-price" name="newProductPrice" step="0.01" min="0.00" placeholder="0.00"><br>
        <label for="new-product-quantity">Quantity:</label><br>
        <input type="number" id="new-product-quantity" name="newProductQuantity" step="0.001" min="0.000" placeholder="0.000"><br>
        <input type="submit" value="Submit">
    </form>
</div>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>
