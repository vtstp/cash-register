<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Commodity Expert" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>

<body>
<ul>
    <li>
        <div>
            <c:url value="controller" var="showAllProducts">
                <c:param name="command" value="showAllProducts"/>
            </c:url>
            <form id="show-products-form" method="post" action="${showAllProducts}">
                <button type="submit" id="showProductsButton" form="show-products-form">Show all products</button>
            </form>
        </div>
    </li>
    <li>
        <c:url value="controller" var="createProduct">
            <c:param name="command" value="showNewProduct"/>
        </c:url>
        <form id="create-product-form" method="post" action="${createProduct}">
            <button type="submit" id="createProductButton" form="create-product-form">Create Product</button>
        </form>
    </li>
</ul>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>

</body>
</html>