<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>

<div>
    <h3>Product: </h3>
    <p>Id: ${productId}</p>
    <p>Name: ${productName}</p>
    <p>Stock Quantity: ${productStockQuantity}</p>
    <p>Stock Price: ${productStockPrice}</p>
</div>
<c:choose>

    <c:when test="${commandName == 'addProductToOrder'}">
        <c:url value="controller" var="addProductCommand">
            <c:param name="command" value="addProductToOrder"/>
            <c:param name="productId" value="${productId}"/>
            <c:param name="productName" value="${productName}"/>
        </c:url>
        <form class="quantity-input-form" method="post" action="${addProductCommand}">
            <input type="number" name="productQuantity" step="0.001" min="0.000" placeholder="0.000">
            <input type="submit" value="Submit">
        </form>
    </c:when>

    <c:when test="${commandName == 'changeProductQuantityInOrder'}">
        <c:url value="controller" var="updateOrderProductCommand">
            <c:param name="command" value="changeProductQuantityInOrder"/>
            <c:param name="productId" value="${productId}"/>
            <c:param name="productName" value="${productName}"/>
        </c:url>
        <form class="quantity-input-form" method="post" action="${updateOrderProductCommand}">
            <fieldset>
                <fieldset>
                    <legend>Previous Quantity</legend>
                    <input type="number" name="prevQuantity" value="${productOrderQuantity}" readonly>
                </fieldset>
                <legend>Previous Price</legend>
                <input type="number" name="prevPrice" value="${productOrderPrice}" readonly>
            </fieldset>
            <input type="number" name="productQuantity" step="0.001" min="0.000">
            <input type="submit" value="Submit">
        </form>
    </c:when>

    <c:when test="${commandName == 'changeProductQuantity'}">
        <c:url value="controller" var="updateStockProductCommand">
            <c:param name="command" value="changeProductQuantity"/>
            <c:param name="productId" value="${productId}"/>
            <c:param name="productName" value="${productName}"/>
            <c:param name="productPrice" value="${productStockPrice}"/>
        </c:url>
        <form class="quantity-input-form" method="post" action="${updateStockProductCommand}">
            <input type="number" name="productQuantity" step="0.001" min="0.000">
            <input type="submit" value="Submit">
        </form>
    </c:when>

    <c:when test="${commandName == 'removeProductFromOrder'}">
        <c:url value="controller" var="removeProductFromOrder">
            <c:param name="command" value="removeProductFromOrder"/>
            <c:param name="productId" value="${productId}"/>
            <c:param name="productName" value="${productName}"/>
            <c:param name="productOrderQuantity" value="${productOrderQuantity}"/>
            <c:param name="productOrderPrice" value="${productOrderPrice}"/>
        </c:url>
        <form class="remove-product-form" method="post" action="${removeProductFromOrder}">
            <input type="submit" value="Remove">
        </form>
    </c:when>

</c:choose>

<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>
