<html>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<c:set var="title" value="Cash Machine" scope="session"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<c:if test="${userRole != null}">
    <jsp:include page="/WEB-INF/jspf/header.jspf"/>
    <div>
        <h3>User Info</h3>
        <p>Role: ${userRole.name}</p>
        <p>Login: ${userLogin}</p>
        <p>First Name: ${userFirstName}</p>
        <p>Last Name: ${userLastName}</p>
    </div>
    <c:choose>

        <c:when test="${userRole.name == 'cashier'}">
            <jsp:include page="cashier/cashier.jsp"/>
        </c:when>

        <c:when test="${userRole.name == 'senior_cashier'}">
            <jsp:include page="seniorcashier/seniorcashier.jsp"/>
        </c:when>

        <c:when test="${userRole.name == 'commodity_expert'}">
            <jsp:include page="commodityexpert/commodityexpert.jsp"/>
        </c:when>

    </c:choose>

</c:if>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>