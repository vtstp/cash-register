<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Order" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>

<c:choose>
    <c:when test="${fn:length(orderProductList) == 0}">Empty order</c:when>
    <c:otherwise>
        <table>
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Select</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td colspan="3">Total:</td>
                <td>${totalPrice}</td>
            </tr>
            </tfoot>
            <tbody>
            <c:forEach var="product" items="${orderProductList}">
                <tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.quantity}</td>
                    <td>${product.price}</td>
                    <td>
                        <c:url value="controller" var="showProduct">
                            <c:param name="command" value="showProduct"/>
                            <c:param name="productId" value="${product.id}"/>
                            <c:param name="productOrderQuantity" value="${product.quantity}"/>
                            <c:param name="productOrderPrice" value="${product.price}"/>
                            <c:param name="source" value="order"/>
                        </c:url>
                        <c:url value="controller" var="removeProduct">
                            <c:param name="command" value="removeProductFromOrder"/>
                            <c:param name="productId" value="${product.id}"/>
                            <c:param name="productName" value="${product.name}"/>
                            <c:param name="productOrderQuantity" value="${product.quantity}"/>
                            <c:param name="productOrderPrice" value="${product.price}"/>
                        </c:url>
                        <c:choose>
                            <c:when test="${userRole.name == 'senior_cashier' && status == 'opened'}">
                                <form class="select-product-form" method="post" action="${removeProduct}">
                                    <input type="submit" value="Remove"/>
                                </form>
                            </c:when>
                            <c:otherwise>
                                <form class="select-product-form" method="post" action="${showProduct}">
                                    <input type="hidden" name="commandName">
                                    <input type="submit" value="Select"
                                           <c:if test="${userRole.name == 'senior_cashier' && status == 'paid'}">disabled</c:if>/>
                                </form>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <br>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${userRole.name == 'cashier'}">
        <div>
            <c:url value="controller" var="showProductList">
                <c:param name="command" value="showAllProducts"/>
            </c:url>
            <form id="add-product-form" method="post" action="${showProductList}">
                <button type="submit" id="addProductButton" form="add-product-form">Select Product to add</button>
            </form>
        </div>
        <div>
            <c:url value="controller" var="closeOrder">
                <c:param name="command" value="closeOrder"/>
            </c:url>
            <form id="payment-form" method="post" action="${closeOrder}">
                <p>Select payment method</p>
                <input type="radio" id="cash" name="payment" value="CASH" checked>
                <label for="cash">Cash</label><br>
                <input type="radio" id="terminal" name="payment" value="TERMINAL">
                <label for="terminal">Terminal</label><br>
                <button type="submit" id="closeOrderButton" value="Submit" form="payment-form">Submit</button>
            </form>
        </div>
    </c:when>
    <c:when test="${userRole.name == 'senior_cashier'}">
        <div>
            <c:url value="controller" var="cancelOrder">
                <c:param name="command" value="cancelOrder"/>
            </c:url>
            <form id="cancel-order-form" method="post" action="${cancelOrder}">
                <button type="submit" id="cancelOrderButton" value="Cancel" form="cancel-order-form">Cancel</button>
            </form>
        </div>
    </c:when>

</c:choose>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>