package edu.epam.vtstp.cashregister.web.command;


import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.bean.ReceiptBean;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class ShowAllReceiptsCommand extends Command {
    private static final Logger LOG = Logger.getLogger(ShowAllReceiptsCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        List<ReceiptBean> receiptList = OrderDao.findReceiptBeans();
        LOG.trace("Extract from DB: receiptList --> " + receiptList);

        receiptList.sort(Comparator.comparingInt(ReceiptBean::getId));

        request.setAttribute("receiptList", receiptList);
        LOG.trace("Set the request attribute: receiptList --> " + receiptList);

        LOG.debug("Command finished");
        return Path.PAGE_RECEIPT_LIST;
    }
}
