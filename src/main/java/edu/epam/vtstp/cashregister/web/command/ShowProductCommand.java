package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.Role;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.dao.ProductDao;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class ShowProductCommand extends Command {
    private static final Logger LOG = Logger.getLogger(ShowProductCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        HttpSession session = request.getSession(false);
        int productId = Integer.parseInt(request.getParameter("productId"));
        ProductDao productDao = new ProductDao();
        Product product = productDao.findById(productId);
        LOG.trace("Found in DB: product --> " + product);

        String productName = product.getName();
        double productStockQuantity = product.getQuantity();
        double productStockPrice = product.getPrice();
        request.setAttribute("productStockQuantity", productStockQuantity);
        LOG.trace("Set request attribute: productStockQuantity --> " + productStockQuantity);

        request.setAttribute("productStockPrice", productStockPrice);
        LOG.trace("Set request attribute: productStockPrice --> " + productStockPrice);

        request.setAttribute("productId", productId);
        LOG.trace("Set request attribute: productId --> " + productId);

        request.setAttribute("productName", productName);
        LOG.trace("Set request attribute: productName --> " + productName);

        String source = request.getParameter("source");
        Role userRole = Role.valueOf(String.valueOf(session.getAttribute("userRole")));

        String commandName = getCommandName(source, userRole);
        LOG.trace("Get request parameter: commandName --> " + commandName);

        switch (commandName) {
            case ("addProductToOrder"): {
                Map<Integer, Double> orderProductsMap = OrderDao.getOrderProductsMap(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));
                if (orderProductsMap.containsKey(productId)) {
                    LOG.trace("Found product[" + productId + "] in order[" + session.getAttribute("openedOrder") + "]. Proceed to command: " + "changeProductQuantityInOrder");

                    double productOrderQuantity = orderProductsMap.get(productId);
                    double productOrderPrice = new ProductDao().calcPrice(productId, productOrderQuantity);
                    request.setAttribute("productOrderQuantity", productOrderQuantity);
                    LOG.trace("Set request attribute: productOrderQuantity --> " + productOrderQuantity);

                    request.setAttribute("productOrderPrice", productOrderPrice);
                    LOG.trace("Set request attribute: productOrderPrice --> " + productOrderPrice);

                    request.setAttribute("commandName", "changeProductQuantityInOrder");
                    LOG.trace("Set request attribute: commandName --> " + "changeProductQuantityInOrder");
                } else {
                    request.setAttribute("commandName", commandName);
                    LOG.trace("Set request attribute: commandName --> " + commandName);
                }
                break;
            }
            case ("changeProductQuantity"): {
                request.setAttribute("commandName", commandName);
                LOG.trace("Set request attribute: commandName --> " + commandName);
                break;
            }
            case ("changeProductQuantityInOrder"):
            case ("removeProductFromOrder"): {
                double productOrderQuantity = Double.parseDouble(request.getParameter("productOrderQuantity"));
                double productOrderPrice = Double.parseDouble(request.getParameter("productOrderPrice"));
                request.setAttribute("productOrderQuantity", productOrderQuantity);
                LOG.trace("Set request attribute: productOrderQuantity --> " + productOrderQuantity);

                request.setAttribute("productOrderPrice", productOrderPrice);
                LOG.trace("Set request attribute: productOrderPrice --> " + productOrderPrice);

                request.setAttribute("commandName", commandName);
                LOG.trace("Set request attribute: commandName --> " + commandName);
                break;
            }
            default: {
                return Path.PAGE_ERROR_PAGE;
            }
        }

        LOG.debug("Command finished");

        return Path.PAGE_PRODUCT;
    }

    private String getCommandName(String source, Role userRole) {
        String commandName = (userRole.equals(Role.CASHIER)) ?
                (commandName = source.equals("order") ? "changeProductQuantityInOrder" : "addProductToOrder") :
                (commandName = source.equals("order") ? "removeProductFromOrder" : "changeProductQuantity");
        return commandName;
    }
}
