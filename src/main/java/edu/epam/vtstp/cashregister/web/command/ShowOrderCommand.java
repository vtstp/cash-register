package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.bean.ProductBean;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ShowOrderCommand extends Command {
    private static final Logger LOG = Logger.getLogger(ShowOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        HttpSession session = request.getSession(false);

        int receiptId;

        if (request.getParameter("receiptId") == null) {
            receiptId = Integer.parseInt(String.valueOf(session.getAttribute("openedOrder")));
        } else {
            receiptId = Integer.parseInt(request.getParameter("receiptId"));
        }

        List<ProductBean> orderProductBeanList = OrderDao.findOrderProductBeans(receiptId);

        OrderDao orderDao = new OrderDao();
        double totalPrice = orderDao.findById(receiptId).getTotalPrice();

        request.setAttribute("receiptId", receiptId);
        LOG.trace("Set request attribute: receiptId --> " + receiptId);

        request.setAttribute("orderProductList", orderProductBeanList);
        LOG.trace("Set request attribute: orderProductList --> " + orderProductBeanList);

        request.setAttribute("totalPrice", totalPrice);
        LOG.trace("Set request attribute: totalPrice --> " + totalPrice);

        LOG.debug("Command finished");

        return Path.PAGE_ORDER;
    }
}
