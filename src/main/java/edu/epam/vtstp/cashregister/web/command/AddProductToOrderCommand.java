package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.dao.ProductDao;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.db.entity.Receipt;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class AddProductToOrderCommand extends Command {
    private static final Logger LOG = Logger.getLogger(AddProductToOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        HttpSession session = request.getSession(false);
        OrderDao orderDao = new OrderDao();
        Product product = new Product();
        Receipt receipt = orderDao.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));

        Map<Integer, Double> orderProductsMap = OrderDao.getOrderProductsMap(receipt.getId());
        LOG.trace("Get map of order products: orderProductsMap --> " + orderProductsMap.toString());

        int id = Integer.parseInt(request.getParameter("productId"));
        product.setId(id);
        LOG.trace("Request parameter: id --> " + product.getId());

        product.setName(request.getParameter("productName"));
        LOG.trace("Request parameter: name --> " + product.getName());

        product.setQuantity(Double.parseDouble(request.getParameter("productQuantity")));
        LOG.trace("Request parameter: quantity --> " + product.getQuantity());

        product.setPrice(new ProductDao().calcPrice(product.getId(), product.getQuantity()));
        LOG.trace("Request parameter: price --> " + product.getPrice());

        orderDao.addProduct(receipt, product);
        LOG.trace("Product added to order: order --> " + OrderDao.findOrderProductBeans(receipt.getId()));

        OrderDao.updateReceipt(receipt);
        LOG.trace("Update receipt: receipt --> " + receipt);

        LOG.debug("Command finished");
        return Path.COMMAND_SHOW_ORDER;
    }
}
