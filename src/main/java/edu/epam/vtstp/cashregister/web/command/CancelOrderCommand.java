package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.Status;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.entity.Receipt;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class CancelOrderCommand extends Command {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    private static final Logger LOG = Logger.getLogger(CancelOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        HttpSession session = request.getSession(false);
        OrderDao orderDao = new OrderDao();

        Receipt receipt =  orderDao.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));
        LOG.trace("Session attribute: openedOrder --> " + receipt);

        orderDao.deleteOrder(receipt);
        LOG.trace("Order deleted from DB");

        receipt.setTimestamp(new Timestamp(System.currentTimeMillis()));
        LOG.trace("Receipt timestamp updated: timestamp --> " + simpleDateFormat.format(receipt.getTimestamp()));

        receipt.setStatus(Status.CANCELED);
        LOG.trace("Receipt status updated: status --> " + receipt.getStatus());

        receipt.setTotalPrice(0.00);
        LOG.trace("Receipt total price updated: total price --> " + receipt.getTotalPrice());

        OrderDao.updateReceipt(receipt);
        LOG.trace("Receipt canceled: receipt --> " + receipt);

        session.removeAttribute("openedOrder");
        LOG.trace("Session attribute: openedOrder --> " + session.getAttribute("openedOrder"));

        LOG.debug("Command finished");
        return Path.COMMAND_SHOW_ALL_RECEIPTS;
    }
}
