package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.Role;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.dao.UserDao;
import edu.epam.vtstp.cashregister.db.entity.User;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginCommand extends Command {
    private static final Logger LOG = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("LoginCommand starts");

        HttpSession session = request.getSession();

        UserDao userDAO = new UserDao();
        String login = request.getParameter("login");
        LOG.trace("Request parameter: login --> " + login);

        String password = request.getParameter("password");
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            throw new AppException("Login/password cannot be empty");
        }

        User user = userDAO.findByName(login);
        LOG.trace("Found in database: user --> " + user);

        if (user == null) {
            throw new AppException("Invalid login");
        }

        if (user.equals(session.getAttribute("loggedInUser"))) {
            throw new AppException("User already logged in");
        }

        if (!password.equals(user.getPassword())) {
            throw new AppException("Invalid password");
        }

        Role userRole = user.getRole();
        LOG.trace("User [" + login + "] role --> " + userRole);

        String forward = Path.PAGE_ERROR_PAGE;

        if (userRole == Role.COMMODITY_EXPERT) {
            forward = Path.PAGE_COMMODITY_EXPERT;
        } else if (userRole == Role.CASHIER) {
            forward = Path.PAGE_CASHIER;
        } else if (userRole == Role.SENIOR_CASHIER) {
            forward = Path.PAGE_SENIOR_CASHIER;
        }

        session.setAttribute("loggedInUser", user.getLogin());
        LOG.trace("Set the session attribute: loggedInUser --> " + user);

        int openedOrder = OrderDao.checkOpenedOrders();
        if (openedOrder != 0) {
            session.setAttribute("openedOrder", openedOrder);
            LOG.trace("Set the session attribute: openedOrder --> " + openedOrder);
        } else {
            LOG.trace("No opened orders found");
        }

        session.setAttribute("userRole", userRole);
        LOG.trace("Set the session attribute: userRole --> " + userRole);

        session.setAttribute("userLogin", user.getLogin());
        LOG.trace("Set the session attribute: userLogin --> " + user.getLogin());

        session.setAttribute("userFirstName", user.getFirstName());
        LOG.trace("Set the session attribute: userFirstName --> " + user.getFirstName());

        session.setAttribute("userLastName", user.getLastName());
        LOG.trace("Set the session attribute: userLastName --> " + user.getLastName());

        LOG.info("User " + user + " logged as " + userRole.toString().toLowerCase());

        LOG.debug("Command finished");

        return forward;
    }
}
