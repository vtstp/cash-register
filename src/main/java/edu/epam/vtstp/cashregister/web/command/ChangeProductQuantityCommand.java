package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.dao.ProductDao;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChangeProductQuantityCommand extends Command {
    private static final Logger LOG = Logger.getLogger(ChangeProductQuantityCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

//        HttpSession session = request.getSession(false);
        ProductDao productDao = new ProductDao();
        Product product = new Product();

        product.setId(Integer.parseInt(request.getParameter("productId")));
        LOG.trace("Request parameter: id --> " + product.getId());

        product.setName(request.getParameter("productName"));
        LOG.trace("Request parameter: name --> " + product.getName());

        product.setQuantity(Double.parseDouble(request.getParameter("productQuantity")));
        LOG.trace("Request parameter: quantity --> " + product.getQuantity());

        product.setPrice(Double.parseDouble(request.getParameter("productPrice")));
        LOG.trace("Request parameter: price --> " + product.getPrice());


        productDao.updateProduct(product);
        LOG.trace("Product on storage updated: product --> " + product);

        LOG.debug("Command finished");

        return Path.COMMAND_SHOW_ALL_PRODUCTS;
    }
}
