package edu.epam.vtstp.cashregister.web.command;

import java.util.Map;
import java.util.TreeMap;

public class CommandContainer {

    private static Map<String, Command> commands = new TreeMap<String, Command>();

    static {
        // Common
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("noCommand", new NoCommand());
        commands.put("showOrder", new ShowOrderCommand());
        commands.put("showAllProducts", new ShowAllProductsCommand());
        commands.put("showProduct", new ShowProductCommand());


        // Commodity Expert
        commands.put("createProduct", new CreateProductCommand());
        commands.put("showNewProduct", new ShowNewProductCommand());
        commands.put("changeProductQuantity", new ChangeProductQuantityCommand());

        // Cashier
        commands.put("createOrder", new CreateOrderCommand());
        commands.put("addProductToOrder", new AddProductToOrderCommand());
        commands.put("changeProductQuantityInOrder", new ChangeProductQuantityInOrderCommand());
        commands.put("closeOrder", new CloseOrderCommand());

        // Senior Cashier
        commands.put("cancelOrder", new CancelOrderCommand());
        commands.put("showAllReceipts", new ShowAllReceiptsCommand());
        commands.put("removeProductFromOrder", new RemoveProductFromOrderCommand());

    }

    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            return commands.get("noCommand");
        }

        return commands.get(commandName);
    }
}
