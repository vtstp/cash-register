package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.Payment;
import edu.epam.vtstp.cashregister.db.Status;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.entity.Receipt;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class CloseOrderCommand extends Command {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    private static final Logger LOG = Logger.getLogger(CloseOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        OrderDao orderDao = new OrderDao();
        HttpSession session = request.getSession(false);
        Receipt receipt = orderDao.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));
        LOG.trace("Session attribute: openedOrder --> " + receipt);

        receipt.setPayment(Payment.valueOf(request.getParameter("payment")));
        LOG.trace("Get request parameter: payment --> " + receipt.getPayment());

        receipt.setTimestamp(new Timestamp(System.currentTimeMillis()));
        LOG.trace("Receipt timestamp updated: timestamp --> " + simpleDateFormat.format(receipt.getTimestamp()));

        receipt.setStatus(Status.PAID);
        LOG.trace("Receipt status updated: status --> " + receipt.getStatus());

        OrderDao.updateReceipt(receipt);
        LOG.trace("Receipt closed: receipt --> " + receipt);

        session.removeAttribute("openedOrder");
        LOG.trace("Remove the session attribute: openedOrder --> " + session.getAttribute("openedOrder"));

        LOG.debug("Command finished");
        return Path.PAGE_HOME;
    }
}
