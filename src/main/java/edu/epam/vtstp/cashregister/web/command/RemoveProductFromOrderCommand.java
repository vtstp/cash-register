package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.db.entity.Receipt;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RemoveProductFromOrderCommand extends Command {
    private static final Logger LOG = Logger.getLogger(RemoveProductFromOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        HttpSession session = request.getSession(false);
        OrderDao orderDao = new OrderDao();

        Receipt receipt = orderDao.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));

        Product product = new Product();

        product.setId(Integer.parseInt(request.getParameter("productId")));
        LOG.trace("Request parameter: productId --> " + product.getId());

        product.setName(request.getParameter("productName"));
        LOG.trace("Request parameter: productName --> " + product.getName());

        product.setPrice(Double.parseDouble(request.getParameter("productOrderPrice")));
        LOG.trace("Request parameter: productOrderPrice --> " + product.getPrice());

        product.setQuantity(Double.parseDouble(request.getParameter("productOrderQuantity")));
        LOG.trace("Request parameter: productOrderQuantity --> " + product.getQuantity());

//        LOGGER.trace("Updated product on storage: product --> " + product);

        orderDao.removeProduct(receipt, product);
        LOG.trace("Product removed from order: order --> " + OrderDao.findOrderProductBeans(receipt.getId()));

        OrderDao.updateReceipt(receipt);
//        updateReceipt(receipt, product.getPrice());
        LOG.trace("Update receipt: receipt --> " + receipt);

        LOG.debug("Command finished");
        return Path.COMMAND_SHOW_ORDER;
    }

}
