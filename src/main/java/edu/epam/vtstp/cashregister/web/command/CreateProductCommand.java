package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.dao.ProductDao;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CreateProductCommand extends Command {
    private static final Logger LOG = Logger.getLogger(CreateProductCommand.class);

    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        String newProductName = request.getParameter("newProductName");
        LOG.trace("Request parameter: newProductName --> " + newProductName);

        double newProductPrice = getPrice(request);
        LOG.trace("Request parameter: newProductPrice --> " + newProductPrice);

        double newProductQuantity = getQuantity(request);
        LOG.trace("Request parameter: newProductQuantity --> " + newProductQuantity);

        ProductDao.createNewProduct(newProductName, newProductPrice, newProductQuantity);

        LOG.debug("Command finished");

        return Path.COMMAND_SHOW_ALL_PRODUCTS;
    }

    private double getPrice(HttpServletRequest request) {
        String reqPrice = request.getParameter("newProductPrice");
        LOG.trace("Get request parameter: price --> " + reqPrice);

        if (reqPrice.contains(",")) {
            reqPrice = reqPrice.replace(",", ".");
            LOG.trace("Delimiter replaced to appropriate: reqPrice --> " + reqPrice);
        }

        double productPrice = Double.parseDouble(reqPrice);
        LOG.trace("Set product parameter: productPrice --> " + productPrice);

        return productPrice;
    }

    private double getQuantity(HttpServletRequest request) {
        String reqQuantity = request.getParameter("newProductQuantity");
        LOG.trace("Get request parameter: quantity --> " + reqQuantity);

        if (reqQuantity.contains(",")) {
            reqQuantity = reqQuantity.replace(",", ".");
            LOG.trace("Delimiter replaced to appropriate: reqQuantity --> " + reqQuantity);
        }

        double productQuantity = Double.parseDouble(reqQuantity);
        LOG.trace("Set product parameter: productQuantity --> " + productQuantity);

        return productQuantity;
    }
}
