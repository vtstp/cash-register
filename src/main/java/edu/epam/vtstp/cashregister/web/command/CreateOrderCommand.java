package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import edu.epam.vtstp.cashregister.db.dao.UserDao;
import edu.epam.vtstp.cashregister.db.entity.Receipt;
import edu.epam.vtstp.cashregister.db.entity.User;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;

public class CreateOrderCommand extends Command {
    private static final Logger LOG = Logger.getLogger(CreateOrderCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Command starts");

        HttpSession session = request.getSession(false);
        UserDao userDao = new UserDao();
        OrderDao orderDao = new OrderDao();

        User user = userDao.findByName(String.valueOf(session.getAttribute("loggedInUser")));
        Receipt receipt = orderDao.createNewReceipt(new Timestamp(System.currentTimeMillis()), user);
        LOG.trace("New order created: receipt --> " + receipt);

        session.setAttribute("openedOrder", receipt.getId());
        LOG.trace("Set the session attribute: openedOrder --> " + receipt);

        request.setAttribute("receiptId", receipt.getId());
        LOG.trace("Set request attribute: receiptId --> " + receipt.getId());

        LOG.debug("Command finished");

        return Path.COMMAND_SHOW_ORDER;
    }
}
