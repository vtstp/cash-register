package edu.epam.vtstp.cashregister.web.command;

import edu.epam.vtstp.cashregister.Path;
import edu.epam.vtstp.cashregister.db.dao.ProductDao;
import edu.epam.vtstp.cashregister.db.entity.Entity;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ShowAllProductsCommand extends Command {

    private static final Logger LOG = Logger.getLogger(ShowAllProductsCommand.class);


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {

        LOG.debug("Command starts");

        ProductDao productDao = new ProductDao();
        List<Product> productList = productDao.getEntities();
        LOG.trace("Found in DB: productBeanList --> " + productList);

        productList.sort(Comparator.comparingInt(Entity::getId));

        request.setAttribute("productList", productList);
        LOG.trace("Set the request attribute: productList --> " + productList);

        LOG.debug("Command finished");

        return Path.PAGE_PRODUCT_LIST;
    }
}
