package edu.epam.vtstp.cashregister;

import edu.epam.vtstp.cashregister.db.dao.OrderDao;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/test")
public class TestController extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(TestController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.debug("test starts");

        OrderDao orderDao = new OrderDao();

        LOG.debug("test finished");
    }

}

