package edu.epam.vtstp.cashregister;

public final class Path {

    public static final String PAGE_HOME = "/WEB-INF/jsp/main.jsp";
    public static final String PAGE_LOGIN = "/login.jsp";
    public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
    public static final String PAGE_COMMODITY_EXPERT = "/WEB-INF/jsp/commodityexpert/commodityexpert.jsp";
    public static final String PAGE_CASHIER = "/WEB-INF/jsp/cashier/cashier.jsp";
    public static final String PAGE_SENIOR_CASHIER = "/WEB-INF/jsp/seniorcashier/seniorcashier.jsp";
    public static final String PAGE_PRODUCT = "/WEB-INF/jsp/product.jsp";
    public static final String PAGE_PRODUCT_LIST = "/WEB-INF/jsp/product_list.jsp";
    public static final String PAGE_NEW_PRODUCT = "/WEB-INF/jsp/commodityexpert/create_product.jsp";
    public static final String PAGE_ORDER = "/WEB-INF/jsp/order.jsp";
    public static final String PAGE_RECEIPT_LIST = "/WEB-INF/jsp/seniorcashier/receipt_list.jsp";

    public static final String COMMAND_SHOW_ALL_PRODUCTS = "/controller?command=showAllProducts";
    public static final String COMMAND_SHOW_ORDER = "/controller?command=showOrder";
    public static final String COMMAND_SHOW_ALL_RECEIPTS = "/controller?command=showAllReceipts";

}
