package edu.epam.vtstp.cashregister.exceptions;

public class Messages {
    public static final String ERR_CANNOT_CLOSE_CONNECTION = "Cannot close a connection";

    public static final String ERR_CANNOT_CLOSE_RESULTSET = "Cannot close a result set";

    public static final String ERR_CANNOT_CLOSE_STATEMENT = "Cannot close a statement";

    public static final String ERR_CANNOT_OBTAIN_LIST_OF_PRODUCTS = "Cannot obtain list of products";
    public static final String ERR_CANNOT_OBTAIN_PRODUCT_BY_ID = "Cannot obtain product by id" ;
    public static final String ERR_CANNOT_OBTAIN_PRODUCT_BY_NAME = "Cannot obtain product by name";
    public static final String ERR_CANNOT_CREATE_NEW_PRODUCT = "Cannot create new product";
    public static final String ERR_CANNOT_UPDATE_PRODUCT = "Cannot update product";
    public static final String ERR_CANNOT_OBTAIN_USER_BY_ID = "Cannot obtain user by id";
    public static final String ERR_CANNOT_OBTAIN_USER_BY_LOGIN = "Cannot obtain user by login";

    public static final String ERR_CANNOT_CREATE_NEW_RECEIPT = "Cannot create new receipt";
    public static final String ERR_CANNOT_OBTAIN_RECEIPT_BY_ID = "CAnnot obtain receipt by id";
    public static final String ERR_CANNOT_OBTAIN_OPENED_ORDERS = "Cannot obtain opened orders";
    public static final String ERR_CANNOT_OBTAIN_LIST_OF_ORDER_PRODUCTS = "Cannot obtain list of order products";
    public static final String ERR_CANNOT_OBTAIN_MAP_OF_ORDER_PRODUCTS = "Cannot obtain map of order products";
    public static final String ERR_CANNOT_ADD_NEW_PRODUCT_TO_ORDER = "Cannot add new product to order";
    public static final String ERR_CANNOT_UPDATE_RECEIPT = "Cannot update receipt";
    public static final String ERR_CANNOT_OBTAIN_PRODUCT_PRICES_FROM_ORDER = "Cannot obtain product prices from order";
    public static final String ERR_CANNOT_DELETE_ORDER = "Cannot delete order";
    public static final String ERR_CANNOT_REMOVE_PRODUCT_FROM_ORDER = "Cannot remove product from order";
    public static final String ERR_CANNOT_OBTAIN_LIST_OF_RECEIPTS = "Cannot obtain list of receipts";
    public static final String ERR_CANNOT_OBTAIN_ORDER_CONTENT = "Cannot obtain order content";
    public static final String ERR_CANNOT_OBTAIN_PRODUCT_FROM_ORDER = "Cannot obtain product from order";
    public static final String ERR_CANNOT_CHANGE_PRODUCT_QUANTITY_IN_ORDER = "Cannot change product quantity in order";
    public static final String ERR_CANNOT_UPDATE_PRODUCT_IN_ORDER = "Cannot update product in order";
}
