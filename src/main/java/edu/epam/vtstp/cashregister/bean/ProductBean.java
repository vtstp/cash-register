package edu.epam.vtstp.cashregister.bean;


import edu.epam.vtstp.cashregister.db.entity.Product;

import java.util.Locale;

public class ProductBean {
    private int id;
    private String name;
    private String price;
    private String quantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = String.format(Locale.US, "%.2f", price);
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = String.format(Locale.US, "%.3f", quantity);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product ["+getId()+"] " + name + ": quantity(" + quantity + "), price(" + price+ ")";
    }

    public ProductBean getBean(Product pro) {
        ProductBean productBean = new ProductBean();
        productBean.setId(pro.getId());
        productBean.setName(pro.getName());
        productBean.setQuantity(pro.getQuantity());
        productBean.setPrice(pro.getPrice());
        return productBean;
    }
}
