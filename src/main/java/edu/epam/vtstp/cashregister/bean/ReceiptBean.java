package edu.epam.vtstp.cashregister.bean;


import edu.epam.vtstp.cashregister.db.entity.Receipt;

import java.util.Locale;

public class ReceiptBean  {
    private int id;
    private String date;
    private String time;
    private String userLogin;
    private String totalPrice;
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = String.format(Locale.US, "%.2f", totalPrice);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Receipt ["+getId()+"] Status: " + status;
    }

    public ReceiptBean getBean(Receipt receipt) {
        ReceiptBean bean = new ReceiptBean();
        bean.setId(receipt.getId());
        bean.setDate(receipt.getDate());
        bean.setTime(receipt.getTime());
        bean.setUserLogin(receipt.getUser().getLogin());
        bean.setTotalPrice(receipt.getTotalPrice());
        bean.setStatus(receipt.getStatus().getName());
        return bean;
    }
}
