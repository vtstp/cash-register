package edu.epam.vtstp.cashregister.db.dao;

import edu.epam.vtstp.cashregister.db.DBManager;
import edu.epam.vtstp.cashregister.db.Fields;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.exceptions.DBException;
import edu.epam.vtstp.cashregister.exceptions.Messages;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDao implements EntityDao {

    private static final String FIND_BY_ID = "SELECT * FROM products WHERE id=?";
    private static final String FIND_BY_NAME = "SELECT * FROM products WHERE name=?";
    private static final String CREATE_NEW_PRODUCT = "INSERT INTO products VALUES(DEFAULT, ?, ?, ?)";
    private static final String FIND_ALL_PRODUCTS = "SELECT * FROM products";
    private static final String UPDATE_PRODUCT = "UPDATE products SET price=?, quantity=? WHERE id=?";


    private static final Logger LOG = Logger.getLogger(ProductDao.class);

    private static final DBManager dbManager = initConPool();

    private static DBManager initConPool() {
        return DBManager.getInstance();
    }

    @Override
    public Product findById(int id) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;

        Product product = null;

        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(FIND_BY_ID);
            ps.setString(1, String.valueOf(id));
            rs = ps.executeQuery();
            if (rs.next()) {
                product = getProduct(rs);
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_PRODUCT_BY_ID, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return product;
    }

    @Override
    public Product findByName(String name) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;

        Product product = null;

        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(FIND_BY_NAME);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                product = getProduct(rs);
            }
            con.commit();
        } catch (SQLException sqlException) {
            LOG.error(Messages.ERR_CANNOT_OBTAIN_PRODUCT_BY_NAME, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_PRODUCT_BY_NAME, sqlException);
        }

        return product;
    }


    public List<Product> getEntities() throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = dbManager.getConnection();

        List<Product> productList = new ArrayList<>();

        try {
            ps = con.prepareStatement(FIND_ALL_PRODUCTS);
            rs = ps.executeQuery();
            while (rs.next()) {
                productList.add(getProduct(rs));
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_LIST_OF_PRODUCTS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIST_OF_PRODUCTS, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return productList;
    }

    private static Product getProduct(ResultSet rs) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt(Fields.ENTITY_ID));
        product.setName(rs.getString(Fields.PRODUCT_NAME));
        product.setPrice(rs.getDouble(Fields.PRODUCT_PRICE));
        product.setQuantity(rs.getDouble(Fields.PRODUCT_QUANTITY));

        return product;
    }

    public static void createNewProduct(String name, double price, double quantity) throws DBException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = dbManager.getConnection();

        Product product = new Product();

        try {
            ps = con.prepareStatement(CREATE_NEW_PRODUCT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, name);
            ps.setDouble(2, quantity);
            ps.setDouble(3, price);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                product.setId(rs.getInt(1));
                product.setName(name);
                product.setPrice(price);
                product.setQuantity(quantity);
            }
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_CREATE_NEW_PRODUCT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_CREATE_NEW_PRODUCT, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }
    }

    public void updateProduct(Product product) throws DBException {
        Connection con = dbManager.getConnection();

        try {
            update(con, product);
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_UPDATE_PRODUCT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_UPDATE_PRODUCT, sqlException);
        } finally {
            dbManager.close(con);
        }
    }

    private void update(Connection con, Product product) throws SQLException, DBException {
        PreparedStatement ps = null;

        try {
            ps = con.prepareStatement(UPDATE_PRODUCT);
            ps.setDouble(1, product.getPrice());
            ps.setDouble(2, product.getQuantity());
            ps.setInt(3, product.getId());
            ps.executeUpdate();
        } finally {
            dbManager.close(ps);
        }
    }

    public double calcPrice(int productId, double productQuantity) throws DBException {
        double productPrice = findById(productId).getPrice();
        return productPrice * productQuantity;
    }

}
