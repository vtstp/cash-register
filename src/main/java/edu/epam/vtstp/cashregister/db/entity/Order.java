package edu.epam.vtstp.cashregister.db.entity;

import java.util.List;

public class Order extends Entity {
    private Receipt receipt;
    private List<Product> products;

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
