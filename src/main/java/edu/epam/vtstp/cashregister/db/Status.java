package edu.epam.vtstp.cashregister.db;

public enum Status {
    OPENED, PAID, CANCELED;

    public String getName() {
        return name().toLowerCase();
    }

}
