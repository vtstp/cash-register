package edu.epam.vtstp.cashregister.db;

public class Fields {
    public static final String ENTITY_ID = "id";

    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FIRST_NAME = "first_name";
    public static final String USER_LAST_NAME = "last_name";
    public static final String USER_ROLE_ID = "roles_id";


    public static final String PRODUCT_NAME = "name";
    public static final String PRODUCT_QUANTITY = "quantity";
    public static final String PRODUCT_PRICE = "price";

    public static final String ORDER_PRODUCTS_ID = "products_id" ;
    public static final String ORDER_PRODUCT_QUANTITY = "product_quantity";
    public static final String ORDER_PRODUCT_PRICE = "product_price";

    public static final String RECEIPT_DATE_CREATE = "date_create";
    public static final String RECEIPT_BILL = "bill";
    public static final String RECEIPT_USER_ID = "users_id";
    public static final String RECEIPT_STATUS_ID = "statuses_id";
    public static final String RECEIPT_PAYMENT_ID = "payments_id";
}
