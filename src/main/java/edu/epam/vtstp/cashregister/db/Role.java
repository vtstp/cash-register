package edu.epam.vtstp.cashregister.db;

public enum Role {
    COMMODITY_EXPERT, CASHIER, SENIOR_CASHIER;

     public String getName() {
        return name().toLowerCase();
    }

}
