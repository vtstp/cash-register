package edu.epam.vtstp.cashregister.db.dao;

import edu.epam.vtstp.cashregister.db.DBManager;
import edu.epam.vtstp.cashregister.db.Fields;
import edu.epam.vtstp.cashregister.db.Role;
import edu.epam.vtstp.cashregister.db.entity.User;
import edu.epam.vtstp.cashregister.exceptions.DBException;
import edu.epam.vtstp.cashregister.exceptions.Messages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao implements EntityDao {
    private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";
    private static final String SQL_FIND_USER_BY_ID = "SELECT * FROM users WHERE id=?";

    private static final DBManager dbManager = initConPool();

    private static DBManager initConPool() {
        return DBManager.getInstance();
    }

    @Override
    public User findById(int id) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = dbManager.getConnection();

        User user = null;

        try {
            ps = con.prepareStatement(SQL_FIND_USER_BY_ID);
            ps.setString(1, String.valueOf(id));
            rs = ps.executeQuery();
            if (rs.next()) {
                user = getUser(rs);
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_USER_BY_ID, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return user;
    }

    @Override
    public User findByName(String login) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = dbManager.getConnection();

        User user = null;

        try {
            ps = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
            ps.setString(1, login);
            rs = ps.executeQuery();
            if (rs.next()) {
                user = getUser(rs);
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_USER_BY_LOGIN, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return user;
    }

    private static User getUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(Fields.ENTITY_ID));
        user.setLogin(resultSet.getString(Fields.USER_LOGIN));
        user.setPassword(resultSet.getString(Fields.USER_PASSWORD));
        user.setFirstName(resultSet.getString(Fields.USER_FIRST_NAME));
        user.setLastName(resultSet.getString(Fields.USER_LAST_NAME));
        user.setRole(Role.values()[resultSet.getInt(Fields.USER_ROLE_ID)]);

        return user;
    }
}
