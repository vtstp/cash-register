package edu.epam.vtstp.cashregister.db;

public enum Payment {
    CASH, TERMINAL;

    public String getName() {
        return name().toLowerCase();
    }
}
