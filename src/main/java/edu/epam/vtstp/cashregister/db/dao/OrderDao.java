package edu.epam.vtstp.cashregister.db.dao;

import edu.epam.vtstp.cashregister.bean.ProductBean;
import edu.epam.vtstp.cashregister.bean.ReceiptBean;
import edu.epam.vtstp.cashregister.db.DBManager;
import edu.epam.vtstp.cashregister.db.Fields;
import edu.epam.vtstp.cashregister.db.Payment;
import edu.epam.vtstp.cashregister.db.Status;
import edu.epam.vtstp.cashregister.db.entity.Entity;
import edu.epam.vtstp.cashregister.db.entity.Product;
import edu.epam.vtstp.cashregister.db.entity.Receipt;
import edu.epam.vtstp.cashregister.db.entity.User;
import edu.epam.vtstp.cashregister.exceptions.DBException;
import edu.epam.vtstp.cashregister.exceptions.Messages;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDao implements EntityDao {

    private static final Logger LOG = Logger.getLogger(OrderDao.class);
    private static final String CREATE_NEW_RECEIPT = "INSERT INTO receipts VALUES(DEFAULT, ?, DEFAULT, ?, 1, 1)";
    private static final String FIND_RECEIPT_BY_ID = "SELECT * FROM receipts WHERE id=?";
    private static final String GET_ORDER_PRODUCTS = "SELECT products_id, product_quantity, product_price FROM orders WHERE receipts_id=?";
    private static final String GET_ALL_OPENED_ORDERS = "SELECT id FROM receipts WHERE statuses_id=1";
    private static final String GET_ORDER_PRODUCTS_MAP = "SELECT products_id, product_quantity FROM orders WHERE receipts_id=?";
    private static final String ADD_NEW_PRODUCT_TO_ORDER = "INSERT INTO orders VALUES(?, ?, ?, ?)";
    private static final String UPDATE_RECEIPT = "UPDATE receipts SET date_create=?, bill=?, payments_id=?, statuses_id=? WHERE id=?";
    private static final String GET_ORDER_PRICES = "SELECT product_price FROM orders WHERE receipts_id=?";
    private static final String DELETE_PRODUCT_FROM_ORDER = "DELETE FROM orders WHERE products_id=? AND receipts_id=?";
    private static final String GET_ALL_RECEIPTS = "SELECT * FROM receipts";
    private static final String GET_PRODUCT_FROM_ORDER = "SELECT product_quantity, product_price FROM orders WHERE products_id=? AND receipts_id=?";
    private static final String UPDATE_PRODUCT_QUANTITY_IN_ORDER = "UPDATE orders SET product_quantity=?, product_price=? WHERE products_id=? AND receipts_id=?";

    private static final DBManager dbManager = initConPool();

    private static DBManager initConPool() {
        return DBManager.getInstance();
    }

    @Override
    public Receipt findById(int id) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;

        Receipt receipt = new Receipt();

        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(FIND_RECEIPT_BY_ID);
            ps.setString(1, String.valueOf(id));
            rs = ps.executeQuery();
            if (rs.next()) {
                receipt = getReceipt(rs);
            }
            con.commit();
        } catch (SQLException sqlException) {
            LOG.error(Messages.ERR_CANNOT_OBTAIN_RECEIPT_BY_ID, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_RECEIPT_BY_ID, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return receipt;
    }


    private Receipt getReceipt(ResultSet resultSet) throws DBException, SQLException {
        UserDao userDao = new UserDao();
        Receipt receipt = new Receipt();

        receipt.setId(resultSet.getInt(Fields.ENTITY_ID));
        receipt.setTimestamp(resultSet.getTimestamp(Fields.RECEIPT_DATE_CREATE));
        receipt.setTotalPrice(resultSet.getDouble(Fields.RECEIPT_BILL));
        receipt.setUser(userDao.findById(resultSet.getInt(Fields.RECEIPT_USER_ID)));
        receipt.setStatus(Status.values()[resultSet.getInt(Fields.RECEIPT_STATUS_ID) - 1]);
        receipt.setPayment(Payment.values()[resultSet.getInt(Fields.RECEIPT_PAYMENT_ID) - 1]);

        return receipt;
    }

    @Override
    public Entity findByName(String name) throws DBException {
        return null;
    }

    public static int checkOpenedOrders() throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = dbManager.getConnection();

        int result = 0;

        try {
            ps = con.prepareStatement(GET_ALL_OPENED_ORDERS);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getInt(Fields.ENTITY_ID);
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_OPENED_ORDERS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_OPENED_ORDERS, sqlException);
        } finally {
            dbManager.close(con);
        }

        return result;
    }

    public static Map<Integer, Double> getOrderProductsMap(int receiptId) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = dbManager.getConnection();

        Map<Integer, Double> orderProductsMap = new HashMap<>();

        try {
            ps = con.prepareStatement(GET_ORDER_PRODUCTS_MAP);
            ps.setInt(1, receiptId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Integer key = rs.getInt(Fields.ORDER_PRODUCTS_ID);
                Double value = rs.getDouble(Fields.ORDER_PRODUCT_QUANTITY);
                orderProductsMap.put(key, value);
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_MAP_OF_ORDER_PRODUCTS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_MAP_OF_ORDER_PRODUCTS, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return orderProductsMap;
    }

    public void addProduct(Receipt receipt, Product product) throws DBException {
        Connection con = dbManager.getConnection();

        try {
            add(con, receipt, product);
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_ADD_NEW_PRODUCT_TO_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_ADD_NEW_PRODUCT_TO_ORDER, sqlException);
        } finally {
            dbManager.close(con);
        }
    }

    private void add(Connection con, Receipt receipt, Product product) throws DBException, SQLException {
        PreparedStatement ps = null;

        try {
            ps = con.prepareStatement(ADD_NEW_PRODUCT_TO_ORDER);
            ps.setInt(1, receipt.getId());
            ps.setInt(2, product.getId());
            ps.setDouble(3, product.getQuantity());
            ps.setDouble(4, product.getPrice());
            ps.executeUpdate();
            updateProduct(product, (product.getQuantity() * -1));
        } finally {
            dbManager.close(ps);
        }
    }

    private void updateProduct(Product orderedProduct, double deltaQuantity) throws DBException {
        ProductDao productDao = new ProductDao();
        Product product = productDao.findById(orderedProduct.getId());
        product.setQuantity(product.getQuantity() + deltaQuantity);
        productDao.updateProduct(product);
    }

    public static List<ProductBean> findOrderProductBeans(int receiptId) throws DBException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = dbManager.getConnection();

        List<ProductBean> beanList = new ArrayList<>();

        try {
            ps = con.prepareStatement(GET_ORDER_PRODUCTS);
            ps.setInt(1, receiptId);
            rs = ps.executeQuery();
            while (rs.next()) {
                beanList.add(extractProductBean(rs));
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_LIST_OF_ORDER_PRODUCTS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIST_OF_ORDER_PRODUCTS, sqlException);
        } finally {
            dbManager.close(con);
        }

        return beanList;
    }

    public Receipt createNewReceipt(Timestamp timestamp, User user) throws DBException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = dbManager.getConnection();

        Receipt receipt = new Receipt();

        try {
            ps = con.prepareStatement(CREATE_NEW_RECEIPT, Statement.RETURN_GENERATED_KEYS);
            ps.setTimestamp(1, timestamp);
            ps.setInt(2, user.getId());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                receipt.setId(rs.getInt(1));
                receipt.setTimestamp(timestamp);
                receipt.setUser(user);
                receipt.setTotalPrice(0.00);
            }
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_CREATE_NEW_RECEIPT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_CREATE_NEW_RECEIPT, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return receipt;
    }

    private static ProductBean extractProductBean(ResultSet rs) throws SQLException, DBException {
        ProductBean productBean = new ProductBean();
        productBean.setId(rs.getInt(Fields.ORDER_PRODUCTS_ID));
        productBean.setName(new ProductDao().findById(productBean.getId()).getName());
        productBean.setQuantity(rs.getDouble(Fields.ORDER_PRODUCT_QUANTITY));
        productBean.setPrice(rs.getDouble(Fields.ORDER_PRODUCT_PRICE));

        return productBean;
    }

    public static void updateReceipt(Receipt receipt) throws DBException {
        LOG.debug("Receipt update started");

        Connection con = dbManager.getConnection();

        try {
            update(con, receipt);
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_UPDATE_RECEIPT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_UPDATE_RECEIPT, sqlException);
        } finally {
            dbManager.close(con);
        }

        LOG.debug("Receipt update finished");
    }

    private static void update(Connection con, Receipt receipt) throws SQLException, DBException {
        PreparedStatement ps = null;

        try {
            ps = con.prepareStatement(UPDATE_RECEIPT);
            ps.setTimestamp(1, receipt.getTimestamp());
            LOG.trace("Set parameter: timestamp --> " + receipt.getTimestamp());
            ps.setDouble(2, getReceiptTotalPrice(receipt.getId()));
            LOG.trace("Set parameter: totalPrice --> " + receipt.getTotalPrice());
            ps.setInt(3, receipt.getPayment().ordinal() + 1);
            LOG.trace("Set parameter: payment --> " + receipt.getPayment().getName());
            ps.setInt(4, (receipt.getStatus().ordinal()) + 1);
            LOG.trace("Set parameter: status --> " + receipt.getStatus().getName());
            ps.setInt(5, receipt.getId());
            ps.executeUpdate();
        } finally {
            dbManager.close(ps);
        }
    }

    private static double getReceiptTotalPrice(int receiptId) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = dbManager.getConnection();

        double totalPrice = 0.00;

        try {
            ps = con.prepareStatement(GET_ORDER_PRICES);
            ps.setInt(1, receiptId);
            rs = ps.executeQuery();
            while (rs.next()) {
                totalPrice += rs.getDouble(Fields.ORDER_PRODUCT_PRICE);
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_PRODUCT_PRICES_FROM_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_PRODUCT_PRICES_FROM_ORDER, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return totalPrice;
    }

    public static List<Product> findOrderProducts(String receiptId) throws DBException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = dbManager.getConnection();

        List<Product> content = new ArrayList<>();

        try {
            ps = con.prepareStatement(GET_ORDER_PRODUCTS);
            ps.setString(1, receiptId);
            rs = ps.executeQuery();
            while (rs.next()) {
                content.add(getProduct((new ProductDao().findById(rs.getInt(Fields.ORDER_PRODUCTS_ID))),
                        rs.getDouble(Fields.ORDER_PRODUCT_QUANTITY),
                        rs.getDouble(Fields.ORDER_PRODUCT_PRICE)));
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_ORDER_CONTENT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ORDER_CONTENT, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return content;
    }

    private static Product getProduct(Product productIns, double quantity, double price) {
        Product product = new Product();
        product.setId(productIns.getId());
        product.setName(productIns.getName());
        product.setQuantity(quantity);
        product.setPrice(price);
        return product;
    }


    public void deleteOrder(Receipt receipt) throws DBException {
        Connection con = dbManager.getConnection();

        try {
            delete(con, receipt);
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_DELETE_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_DELETE_ORDER, sqlException);
        } finally {
            dbManager.close(con);
        }
    }

    private void delete(Connection con, Receipt receipt) throws SQLException, DBException {
        List<Product> products = findOrderProducts(String.valueOf(receipt.getId()));

        for (Product product : products) {
            remove(con, receipt, product);
        }
    }


    public void removeProduct(Receipt receipt, Product product) throws DBException {
        Connection con = dbManager.getConnection();

        try {
            remove(con, receipt, product);
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_REMOVE_PRODUCT_FROM_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_REMOVE_PRODUCT_FROM_ORDER, sqlException);
        } finally {
            dbManager.close(con);
        }
    }

    private void remove(Connection con, Receipt receipt, Product product) throws DBException, SQLException {
        PreparedStatement ps = null;

        try {
            ps = con.prepareStatement(DELETE_PRODUCT_FROM_ORDER);
            ps.setInt(1, product.getId());
            ps.setInt(2, receipt.getId());
            ps.executeUpdate();
            updateProduct(product, product.getQuantity());
        } finally {
            dbManager.close(ps);
        }
    }

    public void updateProductInOrder(Receipt receipt, Product product) throws DBException {
        LOG.debug("Update product starts");

        if (product.getQuantity() == 0) {
            removeProduct(receipt, product);
        } else {
            Connection con = dbManager.getConnection();

            try {
                changeProductQuantity(con, product, receipt);
                con.commit();
            } catch (SQLException sqlException) {
                dbManager.rollback(con);
                LOG.error(Messages.ERR_CANNOT_UPDATE_PRODUCT_IN_ORDER, sqlException);
                throw new DBException(Messages.ERR_CANNOT_UPDATE_PRODUCT_IN_ORDER, sqlException);
            } finally {
                dbManager.close(con);
            }
        }

        LOG.debug("Update product finished");
    }

    public Product findProductInOrder(int receiptId, Product product) throws DBException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = dbManager.getConnection();

        Product productRecord = new Product();
        productRecord.setId(product.getId());
        productRecord.setName(product.getName());

        try {
            ps = con.prepareStatement(GET_PRODUCT_FROM_ORDER);
            ps.setInt(1, product.getId());
            ps.setInt(2, receiptId);
            rs = ps.executeQuery();
            if (rs.next()) {
                productRecord.setQuantity(rs.getDouble(Fields.ORDER_PRODUCT_QUANTITY));
                productRecord.setPrice(rs.getDouble(Fields.ORDER_PRODUCT_PRICE));
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_PRODUCT_FROM_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_PRODUCT_FROM_ORDER, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }

        return productRecord;
    }



    private void changeProductQuantity(Connection con, Product product, Receipt receipt) throws DBException, SQLException {
        LOG.trace("Get parameter: productCurrentState --> " + product);

        Product productPrevState = findProductInOrder(receipt.getId(), product);

        LOG.trace("Get parameter: productCurrentState --> " + product);
        LOG.trace("Get parameter: productPrevState --> " + productPrevState);

        PreparedStatement ps = null;

        double prevQuantity = productPrevState.getQuantity();

        try {
            ps = con.prepareStatement(UPDATE_PRODUCT_QUANTITY_IN_ORDER);
            ps.setDouble(1, product.getQuantity());
            ps.setDouble(2, product.getPrice());
            ps.setInt(3, product.getId());
            ps.setInt(4, receipt.getId());
            ps.executeUpdate();
            updateProduct(product, prevQuantity - product.getQuantity());
        } catch (SQLException sqlException) {
            LOG.error(Messages.ERR_CANNOT_CHANGE_PRODUCT_QUANTITY_IN_ORDER, sqlException);
            throw new SQLException(Messages.ERR_CANNOT_CHANGE_PRODUCT_QUANTITY_IN_ORDER, sqlException);
        } finally {
            dbManager.close(ps);
        }
    }

    public static List<ReceiptBean> findReceiptBeans() throws DBException {
        List<ReceiptBean> beanList = new ArrayList<>();
        List<Receipt> receipts = new OrderDao().getAllReceipts();

        for (Receipt receipt : receipts) {
            ReceiptBean bean = new ReceiptBean();
            bean.setId(receipt.getId());
            bean.setUserLogin(receipt.getUser().getLogin());
            bean.setDate(receipt.getDate());
            bean.setTime(receipt.getTime());
            bean.setStatus(receipt.getStatus().getName());
            bean.setTotalPrice(receipt.getTotalPrice());
            beanList.add(bean);
        }
        return beanList;
    }


    public List<Receipt> getAllReceipts() throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = dbManager.getConnection();

        List<Receipt> receiptList = new ArrayList<>();

        try {
            ps = con.prepareStatement(GET_ALL_RECEIPTS);
            rs = ps.executeQuery();
            while (rs.next()) {
                receiptList.add(getReceipt(rs));
            }
            con.commit();
        } catch (SQLException sqlException) {
            dbManager.rollback(con);
            LOG.error(Messages.ERR_CANNOT_OBTAIN_LIST_OF_RECEIPTS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIST_OF_RECEIPTS, sqlException);
        } finally {
            dbManager.close(con, ps, rs);
        }
        return receiptList;
    }

}
