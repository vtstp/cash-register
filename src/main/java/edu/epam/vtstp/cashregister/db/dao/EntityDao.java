package edu.epam.vtstp.cashregister.db.dao;

import edu.epam.vtstp.cashregister.db.entity.Entity;
import edu.epam.vtstp.cashregister.exceptions.DBException;

public interface EntityDao {
    Entity findById(int id) throws DBException;

    Entity findByName(String name) throws DBException;
}
