package edu.epam.vtstp.cashregister.db.entity;

import java.util.Locale;

public class Product extends Entity {
    private String name;
    private double price;
    private double quantity;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "Product [id: " + getId() + ", name: " + name + "] quantity: " + String.format(Locale.US, "%.3f", quantity) + ", price: " + String.format(Locale.US, "%.2f", price);
    }
}
