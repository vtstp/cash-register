SET NAMES utf8;

DROP DATABASE IF EXISTS cashregister;
CREATE DATABASE cashregister CHARACTER SET utf8 COLLATE utf8_general_ci;

use cashregister;

CREATE TABLE roles(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(20) NOT NULL UNIQUE
);

INSERT INTO roles VALUES(0, 'commodity-expert');
INSERT INTO roles VALUES(1, 'cashier');
INSERT INTO roles VALUES(2, 'senior-cashier');

CREATE TABLE users(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	login VARCHAR(20) NOT NULL UNIQUE,
	password VARCHAR(20) NOT NULL,
	first_name VARCHAR(20) NOT NULL,
	last_name VARCHAR(20) NOT NULL,
	roles_id INTEGER NOT NULL REFERENCES roles(id)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
);

INSERT INTO users VALUES(DEFAULT, 'commodityexp', 'test', 'Carlos', 'King', 0);
INSERT INTO users VALUES(DEFAULT, 'cashier', 'test', 'Jack', 'Jackson', 1);
INSERT INTO users VALUES(DEFAULT, 'seniorcash', 'test', 'Yolo', 'Urban', 2);

CREATE TABLE statuses(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(10) NOT NULL UNIQUE
);

INSERT INTO statuses VALUES(DEFAULT, 'opened');
INSERT INTO statuses VALUES(DEFAULT, 'paid');
INSERT INTO statuses VALUES(DEFAULT, 'canceled');

CREATE TABLE products(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(45) NOT NULL UNIQUE,
	quantity DECIMAL(10,3) NOT NULL DEFAULT 0.000,
    price DECIMAL(10,2) NOT NULL DEFAULT 0.00
);

INSERT INTO products VALUES(DEFAULT, 'potato', 111.000, 11.00);
INSERT INTO products VALUES(DEFAULT, 'carrot', 10.243, 17.00);
INSERT INTO products VALUES(DEFAULT, 'tomato', 120.528, 30.00);
INSERT INTO products VALUES(DEFAULT, 'ice cream "Lakomka" 70g', 100, 25.00);
INSERT INTO products VALUES(DEFAULT, 'meat', 150.128, 150.00);
INSERT INTO products VALUES(DEFAULT, 'bear 0.7l', 70, 50.00);
INSERT INTO products VALUES(DEFAULT, 'chips "Lays"', 88, 30.00);

CREATE TABLE payment_types (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20) NOT NULL UNIQUE
);

INSERT INTO payment_types VALUES(DEFAULT, 'cash');
INSERT INTO payment_types VALUES(DEFAULT, 'terminal');


CREATE TABLE receipts(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	date_create TIMESTAMP NOT NULL,  
	bill DECIMAL(10,2) NOT NULL DEFAULT 0.00,
	users_id INTEGER NOT NULL REFERENCES users(id),
	statuses_id INTEGER NOT NULL REFERENCES statuses(id),
	payments_id INTEGER NOT NULL REFERENCES payment_types(id)
);

CREATE TABLE orders(
	receipts_id INTEGER NOT NULL REFERENCES receipts(id)  ON DELETE CASCADE,
	products_id INTEGER NOT NULL REFERENCES products(id),
    product_quantity DECIMAL(10,3) NOT NULL DEFAULT 0.000,
    product_price DECIMAL(10,2) NOT NULL DEFAULT 0.00
);

SELECT * FROM roles;
SELECT * FROM users;
SELECT * FROM statuses;
SELECT * FROM products;
SELECT * FROM payment_types;



